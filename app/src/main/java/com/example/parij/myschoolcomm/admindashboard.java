package com.example.parij.myschoolcomm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class admindashboard extends AppCompatActivity {
    Button Logout;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        admindashboard.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admindashboard);
        //final Bundle bundle=getIntent().getExtras();

       // drawerLayout=(DrawerLayout)findViewById(R.id.drawerlayout);

       // actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        //drawerLayout.addDrawerListener(actionBarDrawerToggle);
        //actionBarDrawerToggle.syncState();

        Logout=(Button)findViewById(R.id.Logout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);


        getSupportActionBar().setTitle("  Dashboard");
        //toolbar.setNavigationIcon(R.drawable.childprofbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
       /* getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        //getSupportActionBar().setDisplayShowHomeEnabled(true);


        ImageButton ParentProfile=(ImageButton) findViewById(R.id.parentsprofile);
        ImageButton ChildProfile=(ImageButton) findViewById(R.id.childprofile);
        ImageButton emerg=(ImageButton)findViewById(R.id.emergency);
        ImageButton authper=(ImageButton)findViewById(R.id.authorisedperson);
        ImageButton Timetable=(ImageButton)findViewById(R.id.theme);
        ImageButton Announcements=(ImageButton)findViewById(R.id.Announcements);
        ImageButton reqforLeave=(ImageButton)findViewById(R.id.reqforLeave);
        ImageButton HW=(ImageButton)findViewById(R.id.Homework);
        ImageButton hWClassreport=(ImageButton)findViewById(R.id.homeclassreport);



        ParentProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent parent=new Intent(admindashboard.this,parentwindow.class);

                startActivity(parent);
            }
        });
        ChildProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent childprof=new Intent(admindashboard.this,childdetailswindow.class);

                startActivity(childprof);
            }

        });
        emerg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emergperson=new Intent(admindashboard.this,emergencywindow.class);
                startActivity(emergperson);
            }
        });
        authper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent authper=new Intent(admindashboard.this,authorizedwindow.class);

                startActivity(authper);
            }
        });

        hWClassreport.setOnClickListener(new View.OnClickListener()
                                         {
                                             @Override
                                             public void onClick(View view)
                                             {
                                                 Intent intent = new Intent(admindashboard.this,syllabusadmin.class);
                                                 startActivity(intent);


                                             }



                                         }

        );

        Announcements.setOnClickListener(new View.OnClickListener()
                                         {
                                             @Override
                                             public void onClick(View view)
                                             {
                                                 Intent intent = new Intent(admindashboard.this,AnnouncementActivity.class);
                                                 startActivity(intent);


                                             }







                                         }

        );

        HW.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(admindashboard.this,AdminHomeWork.class);
                startActivity(intent);
            }




        });

        Timetable.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View view)
                                         {
                                             Intent intent=new Intent(admindashboard.this,TimetableActivity.class);
                                             // Intent intent=new Intent(Main4Activity.this,AuthorizedPersonActivity.class);
                                             //intent.putExtras(bundle);
                                             startActivity(intent);


                                         }

                                     }



        );

        reqforLeave.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View view)
                                           {
                                               Intent intent=new Intent(admindashboard.this,ReqLeaveActivity.class);
                                               startActivity(intent);

                                           }



                                       }

        );


        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(admindashboard.this);

                // set title
                alertDialogBuilder.setTitle("Logout?");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Click yes to exit!")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity
                                admindashboard.this.finish();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });



    }
}
