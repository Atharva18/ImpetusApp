package com.example.parij.myschoolcomm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Main4Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
   // Button Logout;
    ImageButton SProfile;
    ImageButton PProfile;
    ImageButton Timetable;
    ImageButton Announcements;
    ImageButton reqforLeave;
    ImageButton HW;
    ImageButton AuthPerson;
    ImageButton hWClassreport;
    ImageButton Emergency;
    ImageView nav_head;
    private DrawerLayout drawer;
    FirebaseDatabase database;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    Bundle bundle;
    TextView name;
    TextView rollNo;
    TextView program;
    ImageView photo;

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Main4Activity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        final Bundle bundle=getIntent().getExtras();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawerlayout);

        findViewById(R.id.drawer_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open right drawer

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                else
                    drawer.openDrawer(GravityCompat.START);
            }
        });



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        drawer.addDrawerListener(toggle);

        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        View header=navigationView.getHeaderView(0);
/*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        name = (TextView)header.findViewById(R.id.name);
        rollNo = (TextView)header.findViewById(R.id.roll);
        program = (TextView)header.findViewById(R.id.program);
        photo =(ImageView)header.findViewById(R.id.photo);


        final String username = bundle.getString("Username");

        FirebaseDatabase database;
        if(username.contains("Bloss")) {

            program.setText("Program : Blossoming");
            final int[] flag = {0};
            database=FirebaseDatabase.getInstance();
            DatabaseReference databaseReference= database.getReference("studinfo").child("Blossoming").child("Morning");

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {

                       String name1 = (String) data.child("name").getValue();
                       String username1=(String)data.child("username").getValue();

                       if(username.equals(username1))
                       {
                         flag[0] =1;
                         name.setText("Name : "+name1);
                         rollNo.setText("Batch :  Morning");

                       }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            databaseReference = database.getReference("Images").child("Child_Profile");
            final String user=username;
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {
                        if(user.equals(data.getKey()))
                        {

                            String url = data.getValue().toString();
                            if(url!=null) {
                                Glide.with(getApplicationContext().getApplicationContext()).load(url).into(photo);
                            }

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });





        }

        else if(username.contains("Budd")) {
            program.setText("Program : Budding");


            program.setText("Program : Budding");
            final int[] flag = {0};
            database=FirebaseDatabase.getInstance();
            DatabaseReference databaseReference= database.getReference("studinfo").child("Budding").child("Morning");

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {

                        String name1 = (String) data.child("name").getValue();
                        String username1=(String)data.child("username").getValue();

                        if(username.equals(username1))
                        {
                            flag[0] =1;
                            name.setText("Name : "+name1);
                            rollNo.setText("Batch :  Morning");

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            databaseReference = database.getReference("Images").child("Child_Profile");
            final String user=username;
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {
                        if(user.equals(data.getKey()))
                        {

                            String url = data.getValue().toString();
                            if(url!=null) {
                                Glide.with(getApplicationContext().getApplicationContext()).load(url).into(photo);
                            }

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        else if(username.contains("Flou")) {
            program.setText("Program : Flourishing");


            program.setText("Program : Flourishing");
            final int[] flag = {0};
            database=FirebaseDatabase.getInstance();
            DatabaseReference databaseReference= database.getReference("studinfo").child("Flourishing").child("Morning");

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {

                        String name1 = (String) data.child("name").getValue();
                        String username1=(String)data.child("username").getValue();

                        if(username.equals(username1))
                        {
                            flag[0] =1;
                            name.setText("Name : "+name1);
                            rollNo.setText("Batch :  Morning");

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            databaseReference = database.getReference("Images").child("Child_Profile");
            final String user=username;
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {
                        if(user.equals(data.getKey()))
                        {

                            String url = data.getValue().toString();
                            if(url!=null) {
                                Glide.with(getApplicationContext().getApplicationContext()).load(url).into(photo);
                            }

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });





        }
        else if(username.contains("Seed")) {
            program.setText("Program : Seeding");


            program.setText("Program : Seeding");
            final int[] flag = {0};
            database=FirebaseDatabase.getInstance();
            DatabaseReference databaseReference= database.getReference("studinfo").child("Seeding").child("Morning");

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {

                        String name1 = (String) data.child("name").getValue();
                        String username1=(String)data.child("username").getValue();

                        if(username.equals(username1))
                        {
                            flag[0] =1;
                            name.setText("Name : "+name1);
                            rollNo.setText("Batch :  Morning");

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            databaseReference = database.getReference("Images").child("Child_Profile");
            final String user=username;
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {
                        if(user.equals(data.getKey()))
                        {

                            String url = data.getValue().toString();
                            if(url!=null) {
                                Glide.with(getApplicationContext().getApplicationContext()).load(url).into(photo);
                            }

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });




        }







        Intialise();



        SProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Jumping into student profile",Toast.LENGTH_LONG).show();
                Intent l=new Intent(Main4Activity.this,Main5Activity.class);
                l.putExtras(bundle);
                startActivity(l);
            }
        });
        PProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Parents Profile",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(Main4Activity.this,parentmain.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        HW.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(Main4Activity.this,HomeWorkActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }




        });

        Timetable.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View view)
                                         {
                                             Intent intent=new Intent(Main4Activity.this,TimetableActivity.class);
                                             // Intent intent=new Intent(Main4Activity.this,AuthorizedPersonActivity.class);
                                             //intent.putExtras(bundle);
                                             startActivity(intent);


                                         }

                                     }



        );

        reqforLeave.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View view)
                                           {
                                               Intent intent=new Intent(Main4Activity.this,ReqLeaveActivity.class);
                                               startActivity(intent);

                                           }



                                       }

        );

        Emergency.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent=new Intent(Main4Activity.this,EmergencyActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }



        });

        AuthPerson.setOnClickListener(new View.OnClickListener()
                                      {
                                          @Override
                                          public void onClick(View view)
                                          {
                                              Intent intent= new Intent(Main4Activity.this,AuthorizedPersonActivity.class);

                                              // Intent intent=new Intent(Main4Activity.this,AuthorizedPersonActivity.class);
                                              intent.putExtras(bundle);
                                              startActivity(intent);

                                          }

                                      }

        );

        hWClassreport.setOnClickListener(new View.OnClickListener()
                                         {
                                             @Override
                                             public void onClick(View view)
                                             {
                                                 Intent intent = new Intent(Main4Activity.this,SyllabusCoverage.class);
                                                 intent.putExtras(bundle);
                                                 startActivity(intent);


                                             }



                                         }

        );

        Announcements.setOnClickListener(new View.OnClickListener()
                                         {
                                             @Override
                                             public void onClick(View view)
                                             {
                                                 Intent intent = new Intent(Main4Activity.this,AnnouncementActivity.class);
                                                 startActivity(intent);


                                             }







                                         }






        );

    }

/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(actionBarDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

*/


    public void Intialise()
    {
       // Logout=(Button)findViewById(R.id.Logout);
        SProfile=(ImageButton)findViewById(R.id.childprofile);
        PProfile=(ImageButton)findViewById(R.id.parentsprofile);
        Timetable=(ImageButton)findViewById(R.id.theme);
        Announcements=(ImageButton)findViewById(R.id.Announcements);
        reqforLeave=(ImageButton)findViewById(R.id.reqforLeave);
        HW=(ImageButton)findViewById(R.id.Homework);
        AuthPerson=(ImageButton)findViewById(R.id.authorisedperson);
        hWClassreport=(ImageButton)findViewById(R.id.homeclassreport);
        Emergency=(ImageButton)findViewById(R.id.emergency);
        nav_head=(ImageView)findViewById(R.id.photo);
      //  name=(TextView)findViewById(R.id.name);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        final Bundle bundle=getIntent().getExtras();
        if (id == R.id.resetpassword) {

            Intent intent=new Intent(Main4Activity.this,ResetPassword.class);
            intent.putExtras(bundle);
            startActivity(intent);
            //Toast.makeText(getApplicationContext(), "Reset Password clicked", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.CallUs) {
           // Toast.makeText(getApplicationContext(), "Call Us is clicked", Toast.LENGTH_SHORT).show();

            final TextView phone1,phone2,phone3;
            Button button1,button2,button3;
            android.support.v7.app.AlertDialog.Builder build=new android.support.v7.app.AlertDialog.Builder(Main4Activity.this);
            View mView=getLayoutInflater().inflate(R.layout.calluspopup,null);

            phone1=(TextView)mView.findViewById(R.id.phone1);
            phone2=(TextView)mView.findViewById(R.id.phone2);
            phone3=(TextView)mView.findViewById(R.id.phone3);

            button1=(Button)mView.findViewById(R.id.phoneButton1);
            button2=(Button)mView.findViewById(R.id.phoneButton2);
            button3=(Button)mView.findViewById(R.id.phoneButton3);


            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = phone1.getText().toString();


                        String dial = "tel:" + number;
                        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));



                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = phone2.getText().toString();


                    String dial = "tel:" + number;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));



                }
            });

            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = phone3.getText().toString();


                    String dial = "tel:" + number;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));



                }
            });





            build.setView(mView);
            android.support.v7.app.AlertDialog dialog=build.create();
            dialog.show();




        } else if (id == R.id.notifications) {
            Toast.makeText(getApplicationContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.feedback) {
            Toast.makeText(getApplicationContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();


        } else if (id == R.id.rateus) {
            Toast.makeText(getApplicationContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.logout) {
           // Toast.makeText(getApplicationContext(), "Logout Clicked clicked", Toast.LENGTH_SHORT).show();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main4Activity.this);

            // set title
            alertDialogBuilder.setTitle("Logout?");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Click yes to exit!")
                    .setCancelable(false)
                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            Main4Activity.this.finish();
                        }
                    })
                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();




        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
